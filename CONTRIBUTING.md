# Introduction


We want to share this project and make it available for anyone with similar analysis needs as ours! You are more than welcome to help us in contributing to it!

Please read these guidelines on how you can contribute to the project.
First of all you can contribute by trying out the code! We have tried to explain every step needed for configuration.
Email us if you find something unclear or open an issue if you think you have found a problem in the configuration. 
This project has been written with specific needs in mind, but your needs might be different from ours. Feature requests are more than welcome and merge request can be used to propose one feature a feature you implemented.

# How to contribute

 * Create issues for specific problems that you might encounter. Report all the specifics you think are useful for reproducing your problem.
 * If you think you have found a bug or you think something can be done more efficienctly, open an issue and discuss about it with us.
 * Create issues for any major changes and enhancements that you wish to make. Discuss things transparently and get community feedback.
 * If you want to implement a change, open a merge request. Before merging this will need to be approved by us.
 * If you find yourself wishing for a feature that doesn't exist, open an issue and discuss with us the feature you would like to see, why you need it, and how it should work.

## How to open merge requests
To submit a change, you first fork the project. You implement the changes there and if you are ready, you submit a merge request.
Feel free to email us for technical support. 

# Get in contact with us
If you want to get in contact with us privately, you can do so at the following e-mails

 * julian.garcia.pardinas@cern.ch
 * simone.meloni@cern.ch



