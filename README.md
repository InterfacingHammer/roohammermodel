# RooHammerModel <a name="HAMMERinHistFactory"></a>

Collection of classes and scripts that implement a user-friendly interface between HistFactory/RooFit and the HAMMER tool (http://hammer.physics.lbl.gov).

# Releases

- RooHammerModel v1.0.
    - Tested on CentOS 7, with HAMMER v1.1.0 and ROOT v6.16.

# Table of contents
- [RooHammerModel <a name="HAMMERinHistFactory"></a>](#roohammermodel)
- [Table of contents](#table-of-contents)
- [Introduction <a name="Introduction"></a>](#introduction)
- [Instructions <a name="Instructions"></a>](#instructions)
  - [Requirements <a name="Requirements"></a>](#requirements)
  - [Setting up the environment <a name="SettingEnv"></a>](#setting-up-the-environment)
  - [Compilation of the PDF class <a name="Compilation"></a>](#compilation-of-the-interface-class)
  - [How to develop a fitter using the HistFactory/Hammer interface <a name="Fitter"></a>](#how-to-develop-a-fitter-using-the-histfactoryhammer-interface)
    - [Preprocessing of a sample (external) <a name="Preprocessing"></a>](#preprocessing-of-a-sample)
    - [Instantiation of the RooHammerModel object <a name="Instantiate"></a>](#instantiation-of-the-roohammermodel-object)
    - [Setting up the HistFactory model <a name="HistFactory"></a>](#setting-up-the-histfactory-model)
    - [Tell RooHammerModel what are the observables <a name="Variables"></a>](#tell-roohammermodel-what-are-the-observables)
    - [Insert the variational histogram in the model <a name="Model"></a>](#insert-the-variational-histogram-in-the-model)
    - [Perform the fit to data <a name="Fit"></a>](#perform-the-fit-to-data)

# Instructions <a name="Instructions"></a>

These instructions are aimed at explaining how the interface can be used to insert the variable-shape HAMMER histograms in a fitter which uses HistFactory.

## Requirements <a name="Minimum requirements"></a>

* ROOT installation with RooFit enabled 
* Hammer installation 
* Boost 
* yaml 

The boost and yaml packages can be installed along with Hammer during its compilation.

The root installation should be followed by the sourcing of the environment through the following command line

```
source <ROOT_INSTALLATION_DIR>/bin/thisroot.sh
```

where `ROOT_INSTALLATION_DIR` is the path into which the ROOT package has been installed. This should set the environment variable `ROOTSYS`, that should point to the ROOT installation folder. 

The installation also requires an environment variable, called `HAMMERSYS` being set and pointing to the Hammer installation folder. For doing so:

```
export HAMMERSYS=<HAMMER_INSTALLATION_DIR>
```


## Setting up the environment <a name="SettingEnv"></a>

The environment needed to run the code in this repository can be set up by sourcing the code in the `setup_env` script. This code sets up the environment variables needed by ROOT and HAMMER.
Clone and cd in this repository and issue:

```bash
chmod +x setup_env;
source setup_env;
```


## Compilation of the PDF class <a name="Compilation"></a>

The interface of the class has to be compiled in order to be used inside a fitter. All the files needed for the compilation are included in this repository. The source and include files are stored in the `src` and `include` directories, and the `CMakeLists.txt` is used to compile 

```bash
mkdir build; cd build;
cmake ..;
make;
```

The `CMakeLists.txt` compiles the PDF class and a Fitter example that will be commented in the next section.

### Linking the class to a generic executable

The RooHammerModel class can be used as a standalone class in any executable.
In order to do so,
the RooHammerModel library has to be correctly linked. 
As an example, assuming that the source code for this new executable has been 
put in the same \texttt{src} directory of the RooHammerModel class, 
the following lines can be added to the *CMakeLists.txt* file:

```
add_executable(App ${PROJECT_SOURCE_DIR}/src/<AppSource>.cpp)
target_link_libraries(App ThirdParty::ROOT RooHammerModel)
```

Further and more detailed information on how to link a library to an executable 
can be found in the building system documentation at 
[](https://cmake.org/documentation/).

## How to develop a fitter using the HistFactory/Hammer interface <a name="Fitter"></a>

In this section a typical fitter application developed with HistFactory and the HAMMER interface will be described. A template has been put in the `src` folder (under the name of `Fitter.cpp`). The code contains some place holders that have to be replaced by user files and directories (as indicated), so it is not expected to work out of the box.

### Preprocessing of a sample (external) <a name="Preprocessing"></a>
The first step in order to be able to fit with Hammer is to preprocess the MC sample with the Hammer package and save the results of the preprocessing. This step will save a `.root` file containing the tensors histograms that will be contracted, during the fitting steps, with the choice of the Wilson coefficients and Form Factor parameters the fitter is inspecting. 

Let's assume this step has already been executed and the output has been saved under the name `Hammer_Tensors.root`. 

At the moment, the Hammer preprocessing is not parallelisable in an automatic way. One possible workaround is to divide the MC sample that is being preprocessed in chunks, and preprocess each of them separately. Each of the single Hammer instances will then save a different `.root` file, which can be loaded and merged in a later stage. This merging is being done under the hood by our `RooHammerModel` class.

More details on how to perform this step can be found in the Hammer Manual.

In order to fix the conventions, let's assume that we have preprocessed a $B\to D \mu \nu$ sample. This has been done by declaring the process called `BtoCMuNu` inside the call of the Hammer `includeDecay` function. The name of the hadronic process used during the preprocessing is `BtoD`.

Let's assume that we want to prepare the sample to do a Standard-Model fit (fixed Wilson coefficients), in which we want to measure some form factor parameters. In order to be able to fit the Form Factor parameters, the form factor class set in the `addFFScheme` must be one of the variational classes in Hammer. As can be read in the Hammer manual, these classes assign additional error or uncertainty indices to the form factors. The value of the errors represents the displacement in the first order linearization of the form factors, around the value passed to the class during preprocessing. The form factor values passed to our class, then, represent the difference of the form factor parameters with respect to the central values set during preprocessing.

In this example, we assume we have used the `CLNVar` class in the `addFFScheme` call. The name of the displacement parameters of the `CLNVar` class are the following:

```c++
{delta_RhoSq, delta_R1, delta_R2, delta_R0}
```

The recap of the preprocessing steps relevant for our purposes is reported down below. During the preprocessing of the events with Hammer, one has the possibility to not keep the bin errors. This can come in handy, as will be explained later.

```c++
Hammer::Hammer ham{};
ham.includeDecay("BtoCMuNu");
ham.addFFScheme("Scheme_1", {{"BtoD","CLNVar"}});
// ... 
// It follows addition of the histograms...
ham.keepErrorsInHistogram("histo_noErrors", false);
ham.keepErrorsInHistogram("histo_Errors",   true);
```

### Instantiation of the RooHammerModel object <a name="Instantiate"></a>

The `RooHammerModel` object is the core of the interface, and has to be instanciated at the beginning of a fitting script (as shown in `Fitter.cpp`) for the desired decay(s). It has to be instructed with the information from the Hammer preprocessing and all the parameters which will contain the fit variables have to be passed to it. 

The example shown here will fit a combination of two samples. The fitter will estimate the form factor parameters that affect the template shape of the first sample, `sample_1`. The Wilson Coefficients will be fixed in this example. Let's declare all the Form Factor parameters as `RooRealVars` and let's group them in a RooArgList. These parameters are identified by `RooHammerModel` (and properly passed to the HAMMER machinery) by they conventional HAMMER names. These names are passed to the class in vectors, in the same order as they appear in the 'RooArgList'. This kind of configuration gives the user total flexibility to specify a particular subset of coefficients to be fitted, and give those parameters any desired user name.

```c++

    RooRealVar* delta_RhoSq_var = new RooRealVar("delta_RhoSq","delta_RhoSq",0.,-1.,1.);
    RooRealVar* delta_R1_var = new RooRealVar("delta_R1","delta_R1",0.,-1.,1.);
    RooRealVar* delta_R2_var = new RooRealVar("delta_R2","delta_R2",0.,-1.,1.);
    RooRealVar* delta_R0_var = new RooRealVar("delta_R0","delta_R0",0.,-1.,1.);
    std::vector<RooRealVar*>* FFparamvarsvec = new std::vector<RooRealVar*>{delta_RhoSq_var,delta_R1_var,delta_R2_var,delta_R0_var};
    
    RooArgList FFparamvars = RooArgList("FFparamvars");
    for (unsigned int j=0; j<FFparamvarsvec->size(); ++j){
        FFparamvars.add(*FFparamvarsvec->at(j));;
    }

    std::vector<std::string>* FFparamnames = new std::vector<std::string>{"delta_RhoSq","delta_R1","delta_R2","delta_R2"};
    std::vector<std::string>* WCparamnames = new std::vector<std::string>{};
```

Since no Wilson coefficient is going to be changed during this example fit, the vector containing the names of the Wilson Coefficients has been declared empty.

Then we instantiate the `RooHammerModel` object. 

```c++
    std::vector<std::string>* hammer_file_names = new std::vector<std::string>{};
    hammer_file_names->push_back("Hammer_Tensors.root");

    RooHammerModel* HAMMERPDF;
    HAMMERPDF = new RooHammerModel("Varying_template",
                                   "Varying_template",
                                   "BtoCMuNu",
                                   WCparamnames,
                                   RooArgList(),
                                   RooArgList(), 
                                   "BtoD",
                                   "CLNVar",
                                   FFparamnames,
                                   FFparamvars, 
                                   hammer_file_names, 
                                   "histo_noErrors",
                                   "histo_Errors", 
                                   "Scheme_1");)
```

The signature of the constructor we are using is the following:

```c++
 RooHammerModel::RooHammerModel(const char *name, const char *title, 
                        const std::string& WCprocessname,
                        std::vector<std::string>* _WCparamnames,
                        const RooArgList& _reWCparamlist,
                        const RooArgList& _imWCparamlist,
                        const std::string& FFprocessname,
                        const std::string& FFmodelname,
                        std::vector<std::string>* _FFparamnames,
                        const RooArgList& _FFparamlist,
                        std::vector<std::string>* filenames,
                        const std::string& histoname_noerrors,
                        const std::string& histoname_witherrors,
                        const std::string& schemename)
```

The arguments are:

 * `name` :  name of the object
 * `title`:  title of the object
 * `WCprocessname` :  string representation of the vertex considered (set in includeDecay() during preprocessing)
 * `_WCparamnames` :  names of the wilson coefficients you want to vary in the fitter. If this is empty all the Wilson coefficients are maintained equal to the ones set during the preprocessing
 * `_reWCparamlist`:  list of the variables that will contain the Imaginary Part of the varied Wilson Coefficients
 * `_imWCparamlist`:  list of the variables that will contain the Real Part of the varied Wilson Coefficients
 * `FFprocessname` :  name of the hadronic process (set in setFFScheme() during preprocessing)
 * `_FFparamnames`  :  names of the form factor parameters that you want to vary. This has sense only if a variational form factor class has been used during preprocessing. If this is empty, all the form factor displacement parameters are assumed to be zero during the fit.
 * `_FFparamlist`  :  list of the variables that will contain the form factor displacement parameters
 * `filenames`      :  names of the files in output to the Hammer preprocessing. If more than one file is passed, the model assumes they come from a parallel run of chunks of the same dataset and combines them.
 * `histo_noErrors_name`   :  name of the histogram without the errors
 * `histo_withErrors_name` :  name of the histogram with the errors
 * `scheme_name`           :  name of the Form Factors scheme setup with addFFScheme() during preprocessing.

### Setting up the HistFactory model <a name="HistFactory"></a>

In order to correctly take into account the bin errors for the HAMMER-derived templates, needed for the Barlow-Beeson method in `HistFactory`, the first thing we have to do is to retrieve from our `RooHammerModel` object a histogram that represents the decay distribution for the initial values of the parameters and that includes the bin errors. This histogram can be saved in a temporary file that will be read back by HistFactory at a later stage. In this example the temporary file is called `tmp_fit.root`

```c++
TH3D StartingHistogram = HAMMERPDF->getHistogramWithErrors("starting_template_1");

TFile* qtmp = new TFile("tmp_fit.root","recreate");
qtmp->cd();

StartingHistogram.Write();

qtmp->Close();
```
Then all the measurement object is created. The fit will be performed in one channel with two species. The fit is going to extract the yields of the two samples and the form-factor coefficients for the first sample.

The code assumes that the usual histogram templates are stored in a file called `Templates.root`. The initial `Hammer` template with the bin errors is taken from the file we have saved in the previous step.

```c++
    //------Setup of the measurement
    RooStats::HistFactory::Measurement meas("FitWithHAMMER", "FitWithHAMMER");
    meas.SetExportOnly(1);
    meas.SetLumi(1.0);

    //-----Setup of the Channel
    RooStats::HistFactory::Channel chan("Channel_1");
    chan.SetStatErrorConfig(1.e-5, "Poisson");

    //--Construction of the HistFactory workspace
    RooStats::HistFactory::Sample sample_1("starting_template_1", "starting_template_1", "tmp_fit.root");
    sample_1.SetNormalizeByTheory(0);
    sample_1.AddNormFactor("N_1",  500, 0, 1000.);
    sample_1.ActivateStatError(); // Activate the template uncertainties for this sample
    chan.AddSample(sample_1);

    RooStats::HistFactory::Sample sample_2("template_2", "template_2", "Templates.root");
    sample_2.SetNormalizeByTheory(0);
    sample_2.AddNormFactor("N_2",  250, 0, 500.);
    sample_2.ActivateStatError(); // Activate the template uncertainties for this sample
    chan.AddSample(sample_2);
```

In this example, also the data histogram to be fit is contained in the file called `Templates.root` under the name `DataHistogram`. The following lines instruct HistFactory of the data to fit, to collect all the histograms and to construct the workspace with which the fit is performed. They also extract the `ModelConfig` object with which we will be able to insert our `RooHammerModel` in the measurement later.

```c++
    chan.SetData("DataHistogram", "Templates.root");
    meas.AddChannel(chan);
    meas.CollectHistograms();
    meas.SetPOI("N_1");
    meas.SetPOI("N_2");

    RooWorkspace* w;
    w = RooStats::HistFactory::MakeModelAndMeasurementFast(meas);
    RooStats::ModelConfig *mc = (RooStats::ModelConfig*) w->obj("ModelConfig");
```

At this point the Barlow-Beeston lite method is run and the nuisance parameters associated to each bin have been configured. 

### Tell RooHammerModel what are the observables <a name="Variables"></a>
Our template for the moment is a varying histogram. The observables are not yet linked to the ones of the measurement. In order to tell the `RooHammerModel` instantiation which are the independent variables in the template, we have to use the following method:

```c++
    RooArgSet* obs = (RooArgSet*) mc->GetObservables();
    RooRealVar* x = (RooRealVar*) obs->find("obs_x_Channel_1");
    RooRealVar* y = (RooRealVar*) obs->find("obs_y_Channel_1");
    RooRealVar* z = (RooRealVar*) obs->find("obs_z_Channel_1");

    HAMMERPDF->SetObservables(*x,*y,*z);
```

Now the independent variables are the same. 

### Insert the variational histogram in the model <a name="Model"></a>

At the moment our `HAMMERPDF` object acts like a RooHistPdf whose shape changes with the choice of the Form Factor parameters (and Wilson Coefficients if any was floated).

The PDF is inserted inside the HistFactory model by using a model customizer and substituing the old static template with our new class instantiation.


```c++
    RooSimultaneous *model = (RooSimultaneous*) mc->GetPdf();
    RooCategory* idx = (RooCategory*) obs->find("channelCat");
    RooAbsPdf* pdf_ = model->getPdf(idx->getLabel());
    RooHistFunc* nominal_function = (RooHistFunc*) w->obj("starting_template_1_Channel_1_nominal");

    RooCustomizer* cust_ = new RooCustomizer(*pdf_, "cust");
    cust_->replaceArg(*nominal_function, *HAMMERPDF);
    RooSimultaneous *model_modified = new RooSimultaneous("simPdf_modified", "simPdf_modified", *idx);
    model_modified->addPdf((RooAbsPdf&)*cust_->build(true),idx->getLabel());
    RooStats::HistFactory::HistFactorySimultaneous* model_hf = new RooStats::HistFactory::HistFactorySimultaneous(*model_modified);
```

### Perform the fit to data <a name="Fit"></a>

Now we have inserted the custom template inside our model and the fit can be done.

```c++
    RooAbsData *data = (RooAbsData*) w->data("obsData");

    RooAbsReal* nll = (RooAbsReal*) model_hf->createNLL(*data,RooFit::Offset(1));
    RooMinuit* minuit = new RooMinuit(*nll);
    minuit->setErrorLevel(0.5);
    minuit->setStrategy(2);
    minuit->fit("sh");
```
