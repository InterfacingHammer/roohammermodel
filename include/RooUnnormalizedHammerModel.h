/*****************************************************************************
 *                                                                               *
 * Class: RooUnnormalizedHammerModel                                             *
 *                                                                               *
 * Description: RooFit-based interface to handle an  unnormalised 3D histogram   *
 *    obtained with the HAMMER tool (http://hammer.physics.lbl.gov)              *
 *                                                                               *
 * Authors: Julian Garcia Pardinas, Simone Meloni                                *
 * (julian.garcia.pardinas@cern.ch, simone.meloni@cern.ch)                       *
 *                                                                               *
 ********************************************************************************/

#ifndef ROOUNNORMALIZEDHAMMERMODEL
#define ROOUNNORMALIZEDHAMMERMODEL

#include "RooAbsReal.h"
#include "RooAbsArg.h"
#include "RooRealProxy.h"
#include "RooCategoryProxy.h"
#include "RooAbsReal.h"
#include "RooAbsCategory.h"
#include "RooSetProxy.h"
#include "RooDataHist.h"
#include "RooRealVar.h"
#include "RooFormulaVar.h"
#include "TH3D.h"
#include "Hammer/Hammer.hh"
#include "RooHistFunc.h"

class RooUnnormalizedHammerModel : public RooAbsReal {
public:
  RooUnnormalizedHammerModel() {} ; 
  RooUnnormalizedHammerModel(const char *name, const char *title, // Default constructor for HistFactory, without fit variables
              std::string_view WCprocessname,
              std::vector<std::string>* _WCparamnames,
              const RooArgList& _reWCparamlist,
              const RooArgList& _imWCparamlist,
              std::string_view FFprocessname,
              std::string_view FFmodelname,
              std::vector<std::string>* _FFparamnames,
              const RooArgList& _FFparamlist,
              std::vector<std::string>* filenames,
              std::string_view histoname_noerrors,
              std::string_view histoname_witherrors,
              std::string_view schemename,
              int _mother_id,
              std::vector<int> _daughters_id);
  RooUnnormalizedHammerModel(const char *name, const char *title, // Full constructor, with fit variables
	      RooAbsReal& _obs_x,
	      RooAbsReal& _obs_y,
	      RooAbsReal& _obs_z,
              std::string_view WCprocessname,
              std::vector<std::string>* _WCparamnames,
              const RooArgList& _reWCparamlist,
              const RooArgList& _imWCparamlist,
              std::string_view FFprocessname,
              std::string_view FFmodelname,
              std::vector<std::string>* _FFparamnames,
              const RooArgList& _FFparamlist,
              std::vector<std::string>* filenames,
              std::string_view histoname_noerrors,
              std::string_view histoname_witherrors,
              std::string_view schemename,
              int _mother_id,
              std::vector<int> _daughters_id);
  RooUnnormalizedHammerModel(const RooUnnormalizedHammerModel& other, const char* name=0) ; // Copy constructor
  virtual TObject* clone(const char* newname) const { return new RooUnnormalizedHammerModel(*this,newname); } // Clone constructor
  inline virtual ~RooUnnormalizedHammerModel() { } // Destructor

  // Implementation of the RooAbsPdf methods for analytical integration and model evaluation
  Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const ;
  Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;
  Double_t evaluate() const ;

  bool IsChanged() const; // Check if any of the parameters have been changed with
                          // respect to the last time the function was evaluated

  TH3D getHistogramWithErrors(const char* customname); // Histogram that contains both bin contents and bin uncertainties
  TH3D getHistogram(const char* customname); // Histogram that contains both bin contents and bin uncertainties
  void SetObservables(RooAbsReal& _obs_x, RooAbsReal& _obs_y, RooAbsReal& _obs_z); // Method to set the analysis variables

  double GetUnnormalisedIntegral() const; // Output the (un-normalised) integral of the reweighted model
  double GetRateNum() const; // Output the decay rate for the reweighted model
  double GetRateDen() const; // Output the decay rate for the input model

  void SetExternalMultiplicativeHistogram(TH3D* histo); // Method to define a bin-by-bin multiplicative correction to the model

  void AddEfficiencyVariation(); // Method to activate an overall re-scaling of the histogram by the ratio between the current and initial efficiencies

  // Methods providing information on the model binning, following those of a RooHistFunc class
  virtual std::list<Double_t>* binBoundaries(RooAbsRealLValue& /*obs*/, Double_t /*xlo*/, Double_t /*xhi*/) const ;
  virtual std::list<Double_t>* plotSamplingHint(RooAbsRealLValue& obs, Double_t xlo, Double_t xhi) const;
  virtual Bool_t isBinnedDistribution(const RooArgSet&) const { return 1 ; }


protected:

  // Loading of the HAMMER buffer files
  void loadHammerFiles(std::vector<std::string>* file_names);
  void processHammerFile(const std::string& name_string, bool first_file);

  void perform_reweight(TH3D* histo, bool witherrors) const; // Update of a histogram shape using the current parameter values

  void UpdatePreviousStep() const; // Caching of the current parameter values


private:

  std::shared_ptr<Hammer::Hammer> ham; // Hammer object

  // Fit variables
  RooRealProxy obs_x ;
  RooRealProxy obs_y ;
  RooRealProxy obs_z ;

  // Lists of fit parameters
  RooListProxy reWCparamlist ;
  RooListProxy imWCparamlist ;
  RooListProxy FFparamlist ;

  mutable bool observables_defined;
  mutable bool apply_external_correction;
  mutable bool add_efficiency_variation;

  mutable std::map<std::string,double> reWCparamlist_previous;
  mutable std::map<std::string,double> imWCparamlist_previous;
  mutable std::map<std::string,double> FFparamlist_previous;

  std::string hammerWCprocessname;
  std::vector<std::string>* WCparamnames;
  std::string hammerFFprocessname;
  std::string hammerFFmodelname;
  std::vector<std::string>* FFparamnames;
  std::vector<std::string>* hammerfilenames;
  std::string hammerhistoname_noerrors;
  std::string hammerhistoname_witherrors;
  std::string hammerschemename;

  TH3D* variableHisto; // Cached histogram without bin uncertainties
  TH3D* histo_with_errors; // Cached histogram with bin uncertainties
  TH3D* multiplicativeHisto; // Possible external histogram, used to apply a bin-by-bin multiplicative correction to the model
  TH3D* flatHisto;

  double GetEffCorrection() const; // Output the ratio between the efficiency at the current parameter values and the initial one

  double epsilon;

  int mother_id;
  std::vector<int> daughters_id;

  double init_num_rate;
  double init_unnormalised_integral;

  Int_t nbinsx;
  Int_t nbinsy;
  Int_t nbinsz;

  Double_t Delta_x;
  Double_t Delta_y;
  Double_t Delta_z;

  Double_t x_min;
  Double_t y_min;
  Double_t z_min;

  Double_t x_max;
  Double_t y_max;
  Double_t z_max;

  ClassDef(RooUnnormalizedHammerModel,2)
};
 
#endif
