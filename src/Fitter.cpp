#include <fstream>
#include <stdlib.h>
#include <iostream>
#include "RooHammerModel.h"

#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/HistFactoryNavigation.h"
#include "RooStats/HistFactory/Channel.h"
#include "RooStats/HistFactory/Sample.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"

#include "RooStats/ModelConfig.h"
#include "RooCategory.h"
#include "RooHistFunc.h"
#include "RooCustomizer.h"
#include "RooMinuit.h"


using namespace std;

int main() {

    //-----Setup of the model
    RooRealVar* delta_RhoSq_var = new RooRealVar("delta_RhoSq","delta_RhoSq",0.,-1.,1.);
    RooRealVar* delta_R1_var = new RooRealVar("delta_R1","delta_R1",0.,-1.,1.);
    RooRealVar* delta_R2_var = new RooRealVar("delta_R2","delta_R2",0.,-1.,1.);
    RooRealVar* delta_R0_var = new RooRealVar("delta_R0","delta_R0",0.,-1.,1.);
    std::vector<RooRealVar*>* FFparamvarsvec = new std::vector<RooRealVar*>{delta_RhoSq_var,delta_R1_var,delta_R2_var,delta_R0_var};
    RooArgList FFparamvars = RooArgList("FFparamvars");

    for (unsigned int j=0; j<FFparamvarsvec->size(); ++j){
        FFparamvars.add(*FFparamvarsvec->at(j));;
    }

    std::vector<std::string>* FFparamnames = new std::vector<std::string>{"delta_RhoSq","delta_R1","delta_R2","delta_R2"};
    std::vector<std::string>* WCparamnames = new std::vector<std::string>{};

    std::vector<std::string>* hammer_file_names = new std::vector<std::string>{};
    hammer_file_names->push_back("Hammer_Tensors.root");

    RooHammerModel* HAMMERPDF;
    HAMMERPDF = new RooHammerModel("Varying_template",
                                   "Varying_template",
                                   "BtoCMuNu",
                                   WCparamnames,
                                   RooArgList(),
                                   RooArgList(), 
                                   "BtoD",
                                   "CLNVar",
                                   FFparamnames,
                                   FFparamvars, 
                                   hammer_file_names, 
                                   "histo_noErrors",
                                   "histo_Errors", 
                                   "Scheme_1");

    //------Initial histogram with errors from Hammer
    TH3D StartingHistogram = HAMMERPDF->getHistogramWithErrors("starting_template_1");
    TFile* qtmp = new TFile("tmp_fit.root","recreate");
    qtmp->cd();

    StartingHistogram.Write();

    qtmp->Close();

    //------Setup of the measurement
    RooStats::HistFactory::Measurement meas("FitWithHAMMER", "FitWithHAMMER");
    meas.SetExportOnly(1);
    meas.SetLumi(1.0);

    //-----Setup of the Channel
    RooStats::HistFactory::Channel chan("Channel_1");
    chan.SetStatErrorConfig(1.e-5, "Poisson");

    //--Construction of the HistFactory workspace
    RooStats::HistFactory::Sample sample_1("starting_template_1", "starting_template_1", "tmp_fit.root");
    sample_1.SetNormalizeByTheory(0);
    sample_1.AddNormFactor("N_1",  500, 0, 1000.);
    sample_1.ActivateStatError(); // Activate the template uncertainties for this sample
    chan.AddSample(sample_1);

    RooStats::HistFactory::Sample sample_2("template_2", "template_2", "Templates.root");
    sample_2.SetNormalizeByTheory(0);
    sample_2.AddNormFactor("N_2",  250, 0, 500.);
    sample_2.ActivateStatError(); // Activate the template uncertainties for this sample
    chan.AddSample(sample_2);

    chan.SetData("DataHistogram", "Templates.root");
    meas.AddChannel(chan);
    meas.CollectHistograms();
    meas.SetPOI("N_1");
    meas.SetPOI("N_2");

    RooWorkspace* w;
    w = RooStats::HistFactory::MakeModelAndMeasurementFast(meas);
    RooStats::ModelConfig *mc = (RooStats::ModelConfig*) w->obj("ModelConfig");

    RooArgSet* obs = (RooArgSet*) mc->GetObservables();
    RooRealVar* x = (RooRealVar*) obs->find("obs_x_Channel_1");
    RooRealVar* y = (RooRealVar*) obs->find("obs_y_Channel_1");
    RooRealVar* z = (RooRealVar*) obs->find("obs_z_Channel_1");
    HAMMERPDF->SetObservables(*x,*y,*z);

    RooSimultaneous *model = (RooSimultaneous*) mc->GetPdf();
    RooCategory* idx = (RooCategory*) obs->find("channelCat");
    RooAbsPdf* pdf_ = model->getPdf(idx->getLabel());
    RooHistFunc* nominal_function = (RooHistFunc*) w->obj("starting_template_1_Channel_1_nominal");

    RooCustomizer* cust_ = new RooCustomizer(*pdf_, "cust");
    cust_->replaceArg(*nominal_function, *HAMMERPDF);
    RooSimultaneous *model_modified = new RooSimultaneous("simPdf_modified", "simPdf_modified", *idx);
    model_modified->addPdf((RooAbsPdf&)*cust_->build(true),idx->getLabel());
    RooStats::HistFactory::HistFactorySimultaneous* model_hf = new RooStats::HistFactory::HistFactorySimultaneous(*model_modified);

    RooAbsData *data = (RooAbsData*) w->data("obsData");

    //-----Minimization
    RooAbsReal* nll = (RooAbsReal*) model_hf->createNLL(*data,RooFit::Offset(1));

    RooMinuit* minuit = new RooMinuit(*nll);
    minuit->setErrorLevel(0.5);
    minuit->setStrategy(2);
    minuit->fit("sh");

    return 1;
}
