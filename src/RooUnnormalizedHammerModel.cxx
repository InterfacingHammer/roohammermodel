   /******************************************************************************
 *                                                                               *
 * Class: RooUnnormalizedHammerModel                                             *
 *                                                                               *
 * Description: RooFit-based interface to handle a non normalised 3D histogram   *
 *    obtained with the HAMMER tool (http://hammer.physics.lbl.gov)              *
 *                                                                               *
 * Authors: Julian Garcia Pardinas, Simone Meloni                                *
 * (julian.garcia.pardinas@cern.ch, simone.meloni@cern.ch)                       *
 *                                                                               *
 ********************************************************************************/

#include "Riostream.h"
#include "RooUnnormalizedHammerModel.h"
#include "RooAbsReal.h"
#include "RooAbsCategory.h"
#include "RooArgSet.h"
#include "RooArgList.h"
#include <math.h>
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "RooSetProxy.h"
#include <ctime>

ClassImp(RooUnnormalizedHammerModel) 

 RooUnnormalizedHammerModel::RooUnnormalizedHammerModel(const char *name, const char *title, 
                        std::string_view WCprocessname,
                        std::vector<std::string>* _WCparamnames,
                        const RooArgList& _reWCparamlist,
                        const RooArgList& _imWCparamlist,
                        std::string_view FFprocessname,
                        std::string_view FFmodelname,
                        std::vector<std::string>* _FFparamnames,
                        const RooArgList& _FFparamlist,
                        std::vector<std::string>* filenames,
                        std::string_view histoname_noerrors,
                        std::string_view histoname_witherrors,
                        std::string_view schemename,
                        int _mother_id,
                        std::vector<int> _daughters_id):
   RooAbsReal(name,title),
   reWCparamlist("reWCparamlist","Real part of the Wilson coefficients",this),
   imWCparamlist("imWCparamlist","Imaginary part of the Wilson coefficients",this),
   FFparamlist("FFparamlist","Form-factor parameters",this)
 {

   // Default constructor for HistFactory; the fit variables are not specified

   observables_defined = 0;
   apply_external_correction = 0;
   add_efficiency_variation = 0;

   hammerWCprocessname = WCprocessname;
   WCparamnames = _WCparamnames;
   reWCparamlist.add(_reWCparamlist);
   imWCparamlist.add(_imWCparamlist);

   hammerFFprocessname = FFprocessname;
   hammerFFmodelname = FFmodelname;
   FFparamnames = _FFparamnames;
   FFparamlist.add(_FFparamlist);

   hammerfilenames = filenames;
   hammerhistoname_noerrors = histoname_noerrors;
   hammerhistoname_witherrors = histoname_witherrors;
   hammerschemename = schemename;

   mother_id = _mother_id;
   daughters_id = _daughters_id;

   ham = std::make_shared<Hammer::Hammer>();

   loadHammerFiles(hammerfilenames);

   std::unique_ptr<TH3D> temphisto = ham->getHistogram3D(hammerhistoname_noerrors, hammerschemename+"_noErrors");
   variableHisto = (TH3D*) (temphisto.get())->Clone("variableHisto");

   std::unique_ptr<TH3D> temphisto_2 = ham->getHistogram3D(hammerhistoname_witherrors, hammerschemename+"_withErrors");
   histo_with_errors = (TH3D*) (temphisto_2.get())->Clone("histo_with_errors");

   if (isnan(variableHisto->Integral()) or isnan(histo_with_errors->Integral())) {
      throw std::runtime_error("RooUnnormalizedHammerModel ERROR: The integral of the HAMMER histogram is NAN. Aborting!");
   }

   nbinsx = variableHisto->GetNbinsX();
   nbinsy = variableHisto->GetNbinsY();
   nbinsz = variableHisto->GetNbinsZ();

   Delta_x = variableHisto->GetXaxis()->GetBinWidth(1);
   Delta_y = variableHisto->GetYaxis()->GetBinWidth(1);
   Delta_z = variableHisto->GetZaxis()->GetBinWidth(1);

   x_min = variableHisto->GetXaxis()->GetBinLowEdge(1);
   y_min = variableHisto->GetYaxis()->GetBinLowEdge(1);
   z_min = variableHisto->GetZaxis()->GetBinLowEdge(1);

   x_max = variableHisto->GetXaxis()->GetXmax();
   y_max = variableHisto->GetYaxis()->GetXmax();
   z_max = variableHisto->GetZaxis()->GetXmax();


   epsilon = 1.e-10;
   //epsilon = 1.e-10.;
   flatHisto = (TH3D*) (temphisto.get())->Clone("flatHisto");
   flatHisto->Reset();
   for (int i=1; i<=nbinsx; i++) {
      for (int j=1; j<=nbinsy; j++) {
         for (int k=1; k<=nbinsz; k++) {
            flatHisto->SetBinContent(i,j,k,epsilon);
            flatHisto->SetBinError(i,j,k,0.);
         }
      }
   }
   flatHisto->Sumw2();

   multiplicativeHisto = (TH3D*) (temphisto.get())->Clone("multiplicativeHisto");
   multiplicativeHisto->Reset();

   init_unnormalised_integral = GetUnnormalisedIntegral();
   if (GetRateNum()>0) {init_num_rate = GetRateNum();}
   else {init_num_rate = 0;}

   UpdatePreviousStep();

   perform_reweight(variableHisto,0);

   return;

}

 RooUnnormalizedHammerModel::RooUnnormalizedHammerModel(const char *name, const char *title, 
                        RooAbsReal& _obs_x,
                        RooAbsReal& _obs_y,
                        RooAbsReal& _obs_z,
                        std::string_view WCprocessname,
                        std::vector<std::string>* _WCparamnames,
                        const RooArgList& _reWCparamlist,
                        const RooArgList& _imWCparamlist,
                        std::string_view FFprocessname,
                        std::string_view FFmodelname,
                        std::vector<std::string>* _FFparamnames,
                        const RooArgList& _FFparamlist,
                        std::vector<std::string>* filenames,
                        std::string_view histoname_noerrors,
                        std::string_view histoname_witherrors,
                        std::string_view schemename,
                        int _mother_id,
                        std::vector<int> _daughters_id):
   RooAbsReal(name,title),
   reWCparamlist("reWCparamlist","Real part of the Wilson coefficients",this),
   imWCparamlist("imWCparamlist","Imaginary part of the Wilson coefficients",this),
   FFparamlist("FFparamlist","Form-factor parameters",this)
 {

   // Full constructor, with fit variables

   observables_defined = 1;
   apply_external_correction = 0;
   add_efficiency_variation = 0;

   hammerWCprocessname = WCprocessname;
   WCparamnames = _WCparamnames;
   reWCparamlist.add(_reWCparamlist);
   imWCparamlist.add(_imWCparamlist);

   hammerFFprocessname = FFprocessname;
   hammerFFmodelname = FFmodelname;
   FFparamnames = _FFparamnames;
   FFparamlist.add(_FFparamlist);

   hammerfilenames = filenames;
   hammerhistoname_noerrors = histoname_noerrors;
   hammerhistoname_witherrors = histoname_witherrors;
   hammerschemename = schemename;

   mother_id = _mother_id;
   daughters_id = _daughters_id;

   ham = std::make_shared<Hammer::Hammer>();

   loadHammerFiles(hammerfilenames);

   std::unique_ptr<TH3D> temphisto = ham->getHistogram3D(hammerhistoname_noerrors, hammerschemename+"_noErrors");
   variableHisto = (TH3D*) (temphisto.get())->Clone("variableHisto");

   std::unique_ptr<TH3D> temphisto_2 = ham->getHistogram3D(hammerhistoname_witherrors, hammerschemename+"_withErrors");
   histo_with_errors = (TH3D*) (temphisto_2.get())->Clone("histo_with_errors");

   if (isnan(variableHisto->Integral()) or isnan(histo_with_errors->Integral())) {
      throw std::runtime_error("RooUnnormalizedHammerModel ERROR: The integral of the HAMMER histogram is NAN. Aborting!");
   }

   nbinsx = variableHisto->GetNbinsX();
   nbinsy = variableHisto->GetNbinsY();
   nbinsz = variableHisto->GetNbinsZ();

   Delta_x = variableHisto->GetXaxis()->GetBinWidth(1);
   Delta_y = variableHisto->GetYaxis()->GetBinWidth(1);
   Delta_z = variableHisto->GetZaxis()->GetBinWidth(1);

   x_min = variableHisto->GetXaxis()->GetBinLowEdge(1);
   y_min = variableHisto->GetYaxis()->GetBinLowEdge(1);
   z_min = variableHisto->GetZaxis()->GetBinLowEdge(1);

   x_max = variableHisto->GetXaxis()->GetXmax();
   y_max = variableHisto->GetYaxis()->GetXmax();
   z_max = variableHisto->GetZaxis()->GetXmax();


   SetObservables(_obs_x, _obs_y, _obs_z);

   epsilon = 1.e-10;
   //epsilon = 1.e-10.;
   flatHisto = (TH3D*) (temphisto.get())->Clone("flatHisto");
   flatHisto->Reset();
   for (int i=1; i<=nbinsx; i++) {
      for (int j=1; j<=nbinsy; j++) {
         for (int k=1; k<=nbinsz; k++) {
            flatHisto->SetBinContent(i,j,k,epsilon);
            flatHisto->SetBinError(i,j,k,0.);
         }
      }
   }
   flatHisto->Sumw2();

   multiplicativeHisto = (TH3D*) (temphisto.get())->Clone("multiplicativeHisto");
   multiplicativeHisto->Reset();

   init_unnormalised_integral = GetUnnormalisedIntegral();
   if (GetRateNum()>0) {init_num_rate = GetRateNum();}
   else {init_num_rate = 0;}

   UpdatePreviousStep();

   perform_reweight(variableHisto,0);

   return;

}

 RooUnnormalizedHammerModel::RooUnnormalizedHammerModel(const RooUnnormalizedHammerModel& other, const char* name) :  
   RooAbsReal(other,name),
   reWCparamlist("reWCparamlist",this, other.reWCparamlist),
   imWCparamlist("imWCparamlist",this, other.imWCparamlist),
   FFparamlist("FFparamlist",this,     other.FFparamlist)
 {

   // Copy constructor

   if (other.observables_defined==1) {
      obs_x = RooRealProxy(other.obs_x.GetName(),this,other.obs_x);
      obs_y = RooRealProxy(other.obs_y.GetName(),this,other.obs_y);
      obs_z = RooRealProxy(other.obs_z.GetName(),this,other.obs_z);
      registerProxy(obs_x);
      registerProxy(obs_y);
      registerProxy(obs_z);
      observables_defined = 1;
   }
   else {observables_defined = 0;}

   hammerWCprocessname = other.hammerWCprocessname;
   WCparamnames = other.WCparamnames;
   hammerFFprocessname = other.hammerFFprocessname;
   hammerFFmodelname = other.hammerFFmodelname;
   FFparamnames = other.FFparamnames;
   hammerfilenames = other.hammerfilenames;
   hammerhistoname_noerrors = other.hammerhistoname_noerrors;
   hammerhistoname_witherrors = other.hammerhistoname_witherrors;
   hammerschemename = other.hammerschemename;

   mother_id = other.mother_id;
   daughters_id = other.daughters_id;

   ham = std::shared_ptr<Hammer::Hammer>(other.ham);

   std::unique_ptr<TH3D> temphisto = ham->getHistogram3D(hammerhistoname_noerrors, hammerschemename+"_noErrors");
   variableHisto = (TH3D*) (temphisto.get())->Clone("variableHisto");

   std::unique_ptr<TH3D> temphisto_2 = ham->getHistogram3D(hammerhistoname_witherrors, hammerschemename+"_withErrors");
   histo_with_errors = (TH3D*) (temphisto_2.get())->Clone("histo_with_errors");

   if (isnan(variableHisto->Integral()) or isnan(histo_with_errors->Integral())) {
      throw std::runtime_error("RooUnnormalizedHammerModel ERROR: The integral of the HAMMER histogram is NAN. Aborting!");
   }

   nbinsx = variableHisto->GetNbinsX();
   nbinsy = variableHisto->GetNbinsY();
   nbinsz = variableHisto->GetNbinsZ();

   Delta_x = variableHisto->GetXaxis()->GetBinWidth(1);
   Delta_y = variableHisto->GetYaxis()->GetBinWidth(1);
   Delta_z = variableHisto->GetZaxis()->GetBinWidth(1);

   x_min = variableHisto->GetXaxis()->GetBinLowEdge(1);
   y_min = variableHisto->GetYaxis()->GetBinLowEdge(1);
   z_min = variableHisto->GetZaxis()->GetBinLowEdge(1);

   x_max = variableHisto->GetXaxis()->GetXmax();
   y_max = variableHisto->GetYaxis()->GetXmax();
   z_max = variableHisto->GetZaxis()->GetXmax();

   epsilon = 1.e-10;
   //epsilon = 1.e-10.;
   flatHisto = (TH3D*) (temphisto.get())->Clone("flatHisto");
   flatHisto->Reset();
   for (int i=1; i<=nbinsx; i++) {
      for (int j=1; j<=nbinsy; j++) {
         for (int k=1; k<=nbinsz; k++) {
            flatHisto->SetBinContent(i,j,k,epsilon);
            //flatHisto->SetBinError(i,j,k,0.);
            flatHisto->SetBinError(i,j,k,epsilon);
         }
      }
   }
   flatHisto->Sumw2();

   multiplicativeHisto = (TH3D*) (temphisto.get())->Clone("multiplicativeHisto");
   multiplicativeHisto->Reset();

   apply_external_correction = other.apply_external_correction;
   if (apply_external_correction) {
      SetExternalMultiplicativeHistogram(other.multiplicativeHisto);
   }
   add_efficiency_variation = other.add_efficiency_variation;

   init_unnormalised_integral = GetUnnormalisedIntegral();
   if (GetRateNum()>0) {init_num_rate = GetRateNum();}
   else {init_num_rate = 0;}

   reWCparamlist_previous = std::map<std::string,double>(other.reWCparamlist_previous);
   imWCparamlist_previous = std::map<std::string,double>(other.imWCparamlist_previous);
   FFparamlist_previous   = std::map<std::string,double>(other.FFparamlist_previous);

   perform_reweight(variableHisto,0);

   return;

 } 

 void RooUnnormalizedHammerModel::SetObservables(RooAbsReal& _obs_x, RooAbsReal& _obs_y, RooAbsReal& _obs_z) {

   std::cout << "I AM SETTING THE OBSERVABLES " << std::endl;
   std::cout << "x : " << ((RooRealVar&)_obs_x).getMin() << " , " << ((RooRealVar&)_obs_x).getMax() << std::endl;
   std::cout << "y : " << ((RooRealVar&)_obs_y).getMin() << " , " << ((RooRealVar&)_obs_y).getMax() << std::endl;
   std::cout << "z : " << ((RooRealVar&)_obs_z).getMin() << " , " << ((RooRealVar&)_obs_z).getMax() << std::endl;

   obs_x = RooRealProxy(_obs_x.GetName(),"obs_x",this,_obs_x);
   obs_y = RooRealProxy(_obs_y.GetName(),"obs_y",this,_obs_y);
   obs_z = RooRealProxy(_obs_z.GetName(),"obs_z",this,_obs_z);

   Double_t epsilon_x = (x_max - x_min) * 1.e-8;
   Double_t epsilon_y = (y_max - y_min) * 1.e-8;
   Double_t epsilon_z = (z_max - z_min) * 1.e-8;

   //Throw an error if the boundaries of the  variables are larger than the ones of the Hammer histograms.
   if ( (obs_x.min() - x_min < -epsilon_x ) || (obs_x.max() - x_max > epsilon_x ) || (obs_y.min() - y_min < -epsilon_y ) || (obs_y.max() - y_max > epsilon_y ) || (obs_z.min() - z_min < -epsilon_z ) || (obs_z.max() - z_max > epsilon_z )){
      throw std::runtime_error("The observables you passed have a different range than the ones used in the Hammer preprocessing. Aborting!");
   }

   observables_defined = 1;

   return;

 }

 double RooUnnormalizedHammerModel::GetUnnormalisedIntegral() const {

   if (IsChanged()) {
     perform_reweight(variableHisto,0);
   }
   return ham->getHistogram3D(hammerhistoname_noerrors, hammerschemename+"_noErrors")->Integral();

 }

 double RooUnnormalizedHammerModel::GetRateNum() const {

   if (IsChanged()) {
     perform_reweight(variableHisto,0);
   }
   return ham->getRate(mother_id, daughters_id, hammerschemename+"_noErrors");
   //return ham->getRate(mother_id, daughters_id, hammerschemename+"_withErrors");

 }

 double RooUnnormalizedHammerModel::GetRateDen() const {

   return ham->getDenominatorRate(mother_id, daughters_id);

 }

 double RooUnnormalizedHammerModel::GetEffCorrection() const {

   if (init_num_rate>0) {return GetUnnormalisedIntegral() / init_unnormalised_integral / (GetRateNum() / init_num_rate);}
   else {return 1.;}

 }

 void RooUnnormalizedHammerModel::AddEfficiencyVariation() {

   add_efficiency_variation = 1;

   perform_reweight(variableHisto,0);

   return;

 }

 void RooUnnormalizedHammerModel::SetExternalMultiplicativeHistogram(TH3D* histo) {

   apply_external_correction = 1;

   for (int i=1; i<=nbinsx; i++) {
      for (int j=1; j<=nbinsy; j++) {
         for (int k=1; k<=nbinsz; k++) {
            multiplicativeHisto->SetBinContent(i,j,k,histo->GetBinContent(i,j,k));
            multiplicativeHisto->SetBinError(i,j,k,histo->GetBinError(i,j,k));
         }
      }
   }

   perform_reweight(variableHisto,0);

   return;

 }

 void RooUnnormalizedHammerModel::loadHammerFiles(std::vector<std::string>* file_names) {

   clock_t begin = clock();

   ham->initRun();

   processHammerFile(file_names->at(0),kTRUE);
   for(Int_t i = 1; i != file_names->size(); i++) {processHammerFile(file_names->at(i),kFALSE);}

   clock_t end = clock();
   double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
   std::cout << "RooUnnormalizedHammerModel INFO: Loading of HAMMER files for " << this->GetName() << " (" << elapsed_secs << " s)" << std::endl;

   return;

 }

 void RooUnnormalizedHammerModel::processHammerFile(const std::string& name_string, bool first_file) {

   TFile* inFile = new TFile(name_string.c_str(), "READ");
   if (!inFile) { 
      throw std::runtime_error("RooUnnormalizedHammerModel ERROR: HAMMER buffer file not found. Aborting!");
   }

   TTree* tree;
   inFile->GetObject("Hammer",tree);
   TBranch* brecord = nullptr;
   TBranch* btype = nullptr;
   Hammer::IOBuffer buf{Hammer::RecordType::UNDEFINED, 0ul, new uint8_t[256*1024*1024]};
   tree->SetBranchAddress("record",buf.start, &brecord);
   tree->SetBranchAddress("type",&buf.kind, &btype);
   Long64_t nrecords = tree->GetEntries();

   int histo_num = 0;

   auto entry = tree->LoadTree(0);
   brecord->GetEntry(entry);
   btype->GetEntry(entry);
   if(buf.kind!='b'){
     inFile->Close();
     delete inFile;
     delete[] buf.start;
     throw std::runtime_error("RooUnnormalizedHammerModel ERROR: HAMMER buffer not found. Aborting!");
   }

   for(Int_t i = 0; i<nrecords; ++i){
     entry = tree->LoadTree(i);
     brecord->GetEntry(entry);
     btype->GetEntry(entry);

     if(buf.kind == Hammer::RecordType::HEADER){
       if (first_file and (histo_num==0)) {ham->loadRunHeader(buf,kFALSE);}
       else {ham->loadRunHeader(buf,kTRUE);}
       histo_num += 1;
     }

     else if(buf.kind == Hammer::RecordType::HISTOGRAM_DEFINITION) {
       ham->loadHistogramDefinition(buf,kTRUE);
     }

     else if(buf.kind == Hammer::RecordType::HISTOGRAM) {
       ham->loadHistogram(buf,kTRUE);
     }

     else if(buf.kind == Hammer::RecordType::RATE){
       if (first_file) {ham->loadRates(buf,kTRUE);}
     }
   }

   tree->ResetBranchAddresses();
   inFile->Close();
   delete inFile;
   delete[] buf.start;

 }

 TH3D RooUnnormalizedHammerModel::getHistogramWithErrors(const char* customname)
 {

   perform_reweight(histo_with_errors,1);
   TH3D new_histo = *((TH3D*) histo_with_errors->Clone(customname));

   return new_histo;

 }

  TH3D RooUnnormalizedHammerModel::getHistogram(const char* customname)
 {

   perform_reweight(variableHisto ,0);
   TH3D new_histo = *((TH3D*) variableHisto->Clone(customname));

   return new_histo;

 }

 void RooUnnormalizedHammerModel::Streamer(TBuffer &R__b) {}

 std::list<Double_t>* RooUnnormalizedHammerModel::plotSamplingHint(RooAbsRealLValue& obs, Double_t xlo, Double_t xhi) const 
 {

  if (observables_defined==0) {throw std::runtime_error("RooUnnormalizedHammerModel ERROR: The observables have not been defined. Aborting!");}

  Double_t delta_obs, min_obs;
  Int_t nbins_obs;

  if (std::string(obs_x.GetName())==std::string(obs.GetName())) {
     nbins_obs = nbinsx;
     delta_obs = Delta_x;
     min_obs = x_min;
  }
  else if (std::string(obs_y.GetName())==std::string(obs.GetName())) {
     nbins_obs = nbinsy;
     delta_obs = Delta_y;
     min_obs = y_min;
  }
  else if (std::string(obs_z.GetName())==std::string(obs.GetName())) {
     nbins_obs = nbinsz;
     delta_obs = Delta_z;
     min_obs = z_min;
  }

  Double_t boundaries[nbins_obs+1];
  for (Int_t i=0 ; i<(nbins_obs+1) ; i++) {
     boundaries[i] = min_obs+i*delta_obs;
   }

  std::list<Double_t>* hint = new std::list<Double_t> ;

  Double_t epsilon = (xhi-xlo)*1e-8 ;

  for (Int_t i=0 ; i<(nbins_obs+1) ; i++) {
    if (boundaries[i]>(xlo-epsilon) && boundaries[i]<(xhi+epsilon)) {
       hint->push_back(boundaries[i]-epsilon);
       hint->push_back(boundaries[i]+epsilon);
      }
  }

  return hint ;

 }

 std::list<Double_t>* RooUnnormalizedHammerModel::binBoundaries(RooAbsRealLValue& obs, Double_t xlo, Double_t xhi) const 
 {

  if (observables_defined==0) {throw std::runtime_error("RooUnnormalizedHammerModel ERROR: The observables have not been defined. Aborting!");}

  Double_t delta_obs, min_obs;
  Int_t nbins_obs;

  if (std::string(obs_x.GetName())==std::string(obs.GetName())) {
     nbins_obs = nbinsx;
     delta_obs = Delta_x;
     min_obs = x_min;
  }
  else if (std::string(obs_y.GetName())==std::string(obs.GetName())) {
     nbins_obs = nbinsy;
     delta_obs = Delta_y;
     min_obs = y_min;
  }
  else if (std::string(obs_z.GetName())==std::string(obs.GetName())) {
     nbins_obs = nbinsz;
     delta_obs = Delta_z;
     min_obs = z_min;
  }

  Double_t boundaries[nbins_obs+1];
  for (Int_t i=0 ; i<(nbins_obs+1) ; i++) {
     boundaries[i] = min_obs+i*delta_obs;
   }

  std::list<Double_t>* hint = new std::list<Double_t> ;

  Double_t epsilon = (xhi-xlo)*1e-8 ;

  for (Int_t i=0 ; i<(nbins_obs+1) ; i++) {
    if (boundaries[i]>(xlo-epsilon) && boundaries[i]<(xhi+epsilon)) {
       hint->push_back(boundaries[i]);
      }
  }

  return hint ;

 }

 void RooUnnormalizedHammerModel::perform_reweight(TH3D* histo, bool witherrors) const {

   //std::cout << "I am performing a reweight " << std::endl;
   double proxyRe = 0.;
   double proxyIm = 0.;
   TIterator* iterRe = reWCparamlist.createIterator() ;
   TIterator* iterIm = imWCparamlist.createIterator() ;
   RooAbsArg* argRe;
   RooAbsArg* argIm;

   std::map<std::string, std::complex<double>> WC_map;

   // Update the WC parameter values
   for (int i = 0; i < WCparamnames->size(); ++i) {
     argRe=(RooAbsArg*)iterRe->Next();
     argIm=(RooAbsArg*)iterIm->Next();
     if (dynamic_cast<RooRealVar*>(argRe)) {proxyRe = (dynamic_cast<RooRealVar*>(argRe))->getVal();}
     else {proxyRe = (dynamic_cast<RooFormulaVar*>(argRe))->getVal();}
     if (dynamic_cast<RooRealVar*>(argIm)) {proxyIm = (dynamic_cast<RooRealVar*>(argIm))->getVal();}
     else {proxyIm = (dynamic_cast<RooFormulaVar*>(argIm))->getVal();}
     WC_map.emplace(WCparamnames->at(i), proxyRe + 1i*proxyIm);
     //WC_map.emplace(WCparamnames->at(i), proxyRe);
     //std::cout << " " << proxyRe << " +i " << proxyIm << std::endl;
   }
   //std::cout << hammerWCprocessname << std::endl;
   //for (const auto& [key, value] : WC_map) {
   //     std::cout << key << " = " << value << "; ";
   // }
   ham->setWilsonCoefficients(hammerWCprocessname, WC_map);

   double proxyVal = 0.;
   TIterator* iterVal = FFparamlist.createIterator() ;
   RooAbsArg* argVal;
   std::map<std::string, double> FF_map;

   // Update the FF parameter values   
   for (int i = 0; i < FFparamnames->size(); ++i) {
     argVal=(RooAbsArg*)iterVal->Next();
     if (dynamic_cast<RooRealVar*>(argVal)) {proxyVal = (dynamic_cast<RooRealVar*>(argVal))->getVal();}
     else {proxyVal = (dynamic_cast<RooFormulaVar*>(argVal))->getVal();}
     FF_map.emplace(FFparamnames->at(i), proxyVal);
   }
   ham->setFFEigenvectors(hammerFFprocessname, hammerFFmodelname, FF_map);

   if (witherrors==0) {
      //std::cout << " I am changing the histogram without the errors" << std::endl;
      //std::cout << histo->Integral() << std::endl; 
      ham->setHistogram3D(hammerhistoname_noerrors, hammerschemename+"_noErrors", *histo);
      //std::cout << histo->Integral() << std::endl;
      UpdatePreviousStep();
   }
   else {
      std::cout << " I am changing the histogram with the errors" << std::endl;
      ham->setHistogram3D(hammerhistoname_witherrors, hammerschemename+"_withErrors", *histo);
      histo->Sumw2();
   }

   histo->Add(flatHisto);
   //histo->Scale(1./histo->Integral());

   if (apply_external_correction) {histo->Multiply(multiplicativeHisto);}
   if (add_efficiency_variation) {histo->Scale(GetEffCorrection());}

   delete iterVal;
   delete iterRe;
   delete iterIm;
   
   return; 
 } 

 Int_t RooUnnormalizedHammerModel::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName) const 
 {

   if (observables_defined==0) {throw std::runtime_error("RooUnnormalizedHammerModel ERROR: The observables have not been defined. Aborting!");}
   
   if ( matchArgs(allVars,analVars,obs_x,obs_y,obs_z) ) return 1;
   if ( matchArgs(allVars,analVars,obs_y,obs_z) ) return 2;
   if ( matchArgs(allVars,analVars,obs_x,obs_z) ) return 3;
   if ( matchArgs(allVars,analVars,obs_x,obs_y) ) return 4;
   if ( matchArgs(allVars,analVars,obs_x)       ) return 5;
   if ( matchArgs(allVars,analVars,obs_y)       ) return 6;
   if ( matchArgs(allVars,analVars,obs_z)       ) return 7;

   return 0;

 }

 Double_t RooUnnormalizedHammerModel::analyticalIntegral(Int_t code, const char* rangeName) const 
 {

   if (observables_defined==0) {throw std::runtime_error("RooUnnormalizedHammerModel ERROR: The observables have not been defined. Aborting!");}

   /*if (IsChanged()) {
      int apply_correction_ = apply_external_correction;
      int add_efficiency_variation_ = add_efficiency_variation;
      apply_external_correction = 0;
      add_efficiency_variation = 0;
      perform_reweight(variableHisto,0);
      apply_external_correction = apply_correction_;
      add_efficiency_variation = add_efficiency_variation_;
   }*/

   if (IsChanged()) {
      perform_reweight(variableHisto,0);
   }

   Double_t ret = 0;

   Double_t epsilon_x = (x_max - x_min) * 1.e-8;
   Double_t epsilon_y = (y_max - y_min) * 1.e-8;
   Double_t epsilon_z = (z_max - z_min) * 1.e-8;

   //Evaluate the minimum and the maximum bin of integration
   Double_t x_min_range = obs_x.min(rangeName)+epsilon_x;
   Double_t x_max_range = obs_x.max(rangeName)-epsilon_x;
   Double_t y_min_range = obs_y.min(rangeName)+epsilon_y;
   Double_t y_max_range = obs_y.max(rangeName)-epsilon_y;
   Double_t z_min_range = obs_z.min(rangeName)+epsilon_z;
   Double_t z_max_range = obs_z.max(rangeName)-epsilon_z;

   //Evaluate the number of the bin in which the minimum falls
   Double_t x_min_bin = variableHisto->GetXaxis()->FindBin(x_min_range);
   Double_t y_min_bin = variableHisto->GetYaxis()->FindBin(y_min_range);
   Double_t z_min_bin = variableHisto->GetZaxis()->FindBin(z_min_range);

   //Evaluate the number of the bin in which the maximum fallsM
   Double_t x_max_bin = variableHisto->GetXaxis()->FindBin(x_max_range);
   Double_t y_max_bin = variableHisto->GetYaxis()->FindBin(y_max_range);
   Double_t z_max_bin = variableHisto->GetZaxis()->FindBin(z_max_range);
   
   //Evaluate the Deltas in the bin which has been cut
   Double_t Delta_x_min =  variableHisto->GetXaxis()->GetBinLowEdge(x_min_bin+1) - x_min_range; 
   Double_t Delta_y_min =  variableHisto->GetYaxis()->GetBinLowEdge(y_min_bin+1) - y_min_range; 
   Double_t Delta_z_min =  variableHisto->GetZaxis()->GetBinLowEdge(z_min_bin+1) - z_min_range; 

   Double_t Delta_x_max =  x_max_range - variableHisto->GetXaxis()->GetBinLowEdge(x_max_bin);
   Double_t Delta_y_max =  y_max_range - variableHisto->GetYaxis()->GetBinLowEdge(y_max_bin);
   Double_t Delta_z_max =  z_max_range - variableHisto->GetZaxis()->GetBinLowEdge(z_max_bin);
   
   Double_t delta_x = Delta_x;
   Double_t delta_y = Delta_y;
   Double_t delta_z = Delta_z;

   
   if (code==1) {
      for (int i=x_min_bin; i<=x_max_bin; i++){
         for (int j=y_min_bin; j<=y_max_bin; j++){
            for (int k=z_min_bin; k<=z_max_bin; k++){

               delta_x = Delta_x;
               delta_y = Delta_y;
               delta_z = Delta_z;

               if (i == x_min_bin ) delta_x = Delta_x_min;
               if (i == x_max_bin ) delta_x = Delta_x_max;
               if (j == y_min_bin ) delta_y = Delta_y_min;
               if (j == y_max_bin ) delta_y = Delta_y_max;
               if (k == z_min_bin ) delta_z = Delta_z_min;
               if (k == z_max_bin ) delta_z = Delta_z_max;

               ret += variableHisto->GetBinContent(i,j,k)*delta_x*delta_y*delta_z;

            }
         }
      }
   }
   
   //Index for 1 dimensional integral
   Int_t index_plot_dimension;

   if (code==2) {
       // Return the integral on the y and z variables
      index_plot_dimension = variableHisto->GetXaxis()->FindBin(obs_x);
      for (int i=y_min_bin; i<=y_max_bin; i++) {
         for (int j=1; j<=z_max_bin; j++) {

            delta_y = Delta_y;
            delta_z = Delta_z;

            if (i == y_min_bin ) delta_y = Delta_y_min;
            if (i == y_max_bin ) delta_y = Delta_y_max;
            if (j == z_min_bin ) delta_z = Delta_z_min;
            if (j == z_max_bin ) delta_z = Delta_z_max;

            ret += variableHisto->GetBinContent(index_plot_dimension, i, j)*delta_y*delta_z;
         }
      }
   }

   if (code==3) {
       // Return the integral on the y and z variables
      index_plot_dimension = variableHisto->GetYaxis()->FindBin(obs_y);
      for (int i=x_min_bin; i<=x_max_bin; i++) {
         for (int j=z_min_bin; j<=z_max_bin; j++) {

            delta_x = Delta_x;
            delta_z = Delta_z;

            if (i == x_min_bin ) delta_x = Delta_x_min;
            if (i == x_max_bin ) delta_x = Delta_x_max;
            if (j == z_min_bin ) delta_z = Delta_z_min;
            if (j == z_max_bin ) delta_z = Delta_z_max;

            ret += variableHisto->GetBinContent(i, index_plot_dimension, j)*delta_x*delta_z;
         }
      }
   }
     
   if (code==4) {
       // Return the integral on the x and y variables
      index_plot_dimension = variableHisto->GetZaxis()->FindBin(obs_z);
      for (int i=x_min_bin; i<=x_max_bin; i++) {
         for (int j=y_min_bin; j<=y_max_bin; j++) {

            delta_x = Delta_x;
            delta_y = Delta_y;

            if (i == x_min_bin ) delta_x = Delta_x_min;
            if (i == x_max_bin ) delta_x = Delta_x_max;
            if (j == y_min_bin ) delta_y = Delta_y_min;
            if (j == y_max_bin ) delta_y = Delta_y_max;

            ret += variableHisto->GetBinContent(i, j, index_plot_dimension)*delta_x*delta_y;
         }
      }
   }

   //Indices for 2 dimensional integrals
   Int_t index_plot_dimension_1;
   Int_t index_plot_dimension_2;

   if (code==5) {
      // Return the integral on the x variable
      index_plot_dimension_1 = variableHisto->GetYaxis()->FindBin(obs_y);
      index_plot_dimension_2 = variableHisto->GetZaxis()->FindBin(obs_z);

      for (int i=x_min_bin; i<=x_max_bin; i++) {
         
         delta_x = Delta_x;
         
         if (i==x_min ) delta_x = Delta_x_min;
         if (i==x_max ) delta_x = Delta_x_max;

         ret += variableHisto->GetBinContent(i, index_plot_dimension_1, index_plot_dimension_2)*delta_x;

      }
      /*
      ret += variableHisto->GetBinContent(x_min_bin, index_plot_dimension_1, index_plot_dimension_2)*Delta_x_min;
      for (int i=x_min_bin; i<=x_max_bin; i++) {
         ret += variableHisto->GetBinContent(i, index_plot_dimension_1, index_plot_dimension_2)*Delta_x;
      }
      ret += variableHisto->GetBinContent(x_max_bin, index_plot_dimension_1, index_plot_dimension_2)*Delta_x_max;
      */
   }

   if (code==6) {
      // Return the integral on the y variable
      index_plot_dimension_1 = variableHisto->GetXaxis()->FindBin(obs_x);
      index_plot_dimension_2 = variableHisto->GetZaxis()->FindBin(obs_z);

      for (int i=y_min_bin; i<=y_max_bin; i++) {
         
         delta_y = Delta_y;
         
         if (i==y_min ) delta_y = Delta_y_min;
         if (i==y_max ) delta_y = Delta_y_max;

         ret += variableHisto->GetBinContent(index_plot_dimension_1, i, index_plot_dimension_2)*delta_y;

      }
      /*
      ret += variableHisto->GetBinContent(index_plot_dimension_1, y_min_bin, index_plot_dimension_2)*Delta_y_min;
      for (int i=y_min_bin; i<=y_max_bin; i++) {
         ret += variableHisto->GetBinContent(index_plot_dimension_1, i, index_plot_dimension_2)*Delta_y;
      }
      ret += variableHisto->GetBinContent(index_plot_dimension_1, y_max_bin, index_plot_dimension_2)*Delta_y_max;
      */
   }

   if (code==7) {
      // Return the integral on the y variable
      index_plot_dimension_1 = variableHisto->GetXaxis()->FindBin(obs_x);
      index_plot_dimension_2 = variableHisto->GetYaxis()->FindBin(obs_y);


      for (int i=z_min_bin; i<=z_max_bin; i++) {
         
         delta_z = Delta_z;
         
         if (i==z_min ) delta_z = Delta_z_min;
         if (i==z_max ) delta_z = Delta_z_max;

         ret += variableHisto->GetBinContent(index_plot_dimension_1, index_plot_dimension_2, i)*delta_z;

      }
      /*
      ret += variableHisto->GetBinContent(index_plot_dimension_1, index_plot_dimension_2, z_min_bin)*Delta_z_min;
      for (int i=z_min_bin; i<=z_max_bin; i++) {
         ret += variableHisto->GetBinContent(index_plot_dimension_1, index_plot_dimension_2, i)*Delta_z;
      }
      ret += variableHisto->GetBinContent(index_plot_dimension_1, index_plot_dimension_2, z_max_bin)*Delta_z_max;
      */
      
   }
   return ret;
 }

 Double_t RooUnnormalizedHammerModel::evaluate() const
 { 

   if (observables_defined==0) {throw std::runtime_error("RooUnnormalizedHammerModel ERROR: The observables have not been defined. Aborting!");}

   // Check if any of the relevant parameters for HAMMER has been changed
   if (IsChanged()) {
     perform_reweight(variableHisto,0);
   }

   Double_t val;

   Double_t epsilon_x = (x_max - x_min) * 1.e-8;
   Double_t epsilon_y = (y_max - y_min) * 1.e-8;
   Double_t epsilon_z = (z_max - z_min) * 1.e-8;

   // If the function is outside the Hammer histos boundaries, return a small number
   if ( (obs_x - x_min < -epsilon_x ) || (obs_x - x_max > epsilon_x ) || (obs_y - y_min < -epsilon_y ) || (obs_y - y_max > epsilon_y ) || (obs_z - z_min < -epsilon_z ) || (obs_z - z_max > epsilon_z )){ val = 0.;}
   else{ 
      val = variableHisto->GetBinContent(variableHisto->FindBin(obs_x,obs_y,obs_z));
      //std::cout << "value @ " << obs_x << " , " << obs_y << " , " << obs_z << " = " << val <<std::endl;
      if (val==0) val = 1.e-12; 
   }

   return  val;

 }

bool RooUnnormalizedHammerModel::IsChanged() const
 {

   RooAbsArg* argRe;
   RooAbsArg* argIm;
   RooAbsArg* argFF;
   double argRe_previous;
   double argIm_previous;
   double argFF_previous;

   //std::cout << "I am looking at the change in parameters" << std::endl;

   // Check if the WC have changed
   TIterator* iterRe = reWCparamlist.createIterator() ;
   while ((argRe = (RooAbsArg*) iterRe->Next())){
      if (dynamic_cast<RooRealVar*>(argRe)){
         argRe_previous = reWCparamlist_previous[argRe->GetName()];
         if (!Hammer::isZero(argRe_previous - (dynamic_cast<RooRealVar*>(argRe))->getVal())){
           delete iterRe;
           return kTRUE;
         }
      }
      else{
         argRe_previous = reWCparamlist_previous[argRe->GetName()];
         if (!Hammer::isZero(argRe_previous - (dynamic_cast<RooFormulaVar*>(argRe))->getVal())){
           delete iterRe;
           return kTRUE;
         }
      }
   }

   TIterator* iterIm = imWCparamlist.createIterator() ;
   while ((argIm = (RooAbsArg*) iterIm->Next())){
      if (dynamic_cast<RooRealVar*>(argIm)){
         argIm_previous = imWCparamlist_previous[argIm->GetName()];
         if (!Hammer::isZero(argIm_previous - (dynamic_cast<RooRealVar*>(argIm))->getVal())){
           delete iterRe;
           delete iterIm;
           return kTRUE;
         }
      }
      else{
         argIm_previous = imWCparamlist_previous[argIm->GetName()];
         if (!Hammer::isZero(argIm_previous - (dynamic_cast<RooFormulaVar*>(argIm))->getVal())){
           delete iterRe;
           delete iterIm;
           return kTRUE;
         }
      }
   }

   //Check if the FF parameters have changed
   TIterator* iterFF = FFparamlist.createIterator() ;
   while ((argFF = (RooAbsArg*) iterFF->Next())){
      if (dynamic_cast<RooRealVar*>(argFF)){
         argFF_previous = FFparamlist_previous[argFF->GetName()];
         if (!Hammer::isZero(argFF_previous - (dynamic_cast<RooRealVar*>(argFF))->getVal())){
           delete iterRe;
           delete iterIm;
           delete iterFF;
           return kTRUE;
         }
      }
      else{
         argFF_previous = FFparamlist_previous[argFF->GetName()];
         if (!Hammer::isZero(argFF_previous - (dynamic_cast<RooFormulaVar*>(argFF))->getVal())){
           delete iterRe;
           delete iterIm;
           delete iterFF;
           return kTRUE;
         }
      }
   }

   delete iterRe;
   delete iterIm;
   delete iterFF;

   return kFALSE;

 }

 void RooUnnormalizedHammerModel::UpdatePreviousStep() const{

   double proxyRe  = 0.;
   double proxyIm  = 0.;
   double proxyVal = 0.;

   RooAbsArg* argRe;
   RooAbsArg* argIm;
   RooAbsArg* argFF;

   reWCparamlist_previous.clear();
   TIterator* iterRe = reWCparamlist.createIterator();
   imWCparamlist_previous.clear();
   TIterator* iterIm = imWCparamlist.createIterator();

   for (int i = 0; i < WCparamnames->size(); ++i) {
     argRe=(RooAbsArg*)iterRe->Next();
     argIm=(RooAbsArg*)iterIm->Next();
     if (dynamic_cast<RooRealVar*>(argRe)) {proxyRe = (dynamic_cast<RooRealVar*>(argRe))->getVal();}
     else {proxyRe = (dynamic_cast<RooFormulaVar*>(argRe))->getVal();}
     if (dynamic_cast<RooRealVar*>(argIm)) {proxyIm = (dynamic_cast<RooRealVar*>(argIm))->getVal();}
     else {proxyIm = (dynamic_cast<RooFormulaVar*>(argIm))->getVal();}
     
     reWCparamlist_previous.emplace(argRe->GetName(), proxyRe);
     imWCparamlist_previous.emplace(argIm->GetName(), proxyIm);
   }

   FFparamlist_previous.clear();
   TIterator* iterFF = FFparamlist.createIterator();
   for (int i = 0; i < FFparamnames->size(); ++i) {
     argFF=(RooAbsArg*)iterFF->Next();
     if (dynamic_cast<RooRealVar*>(argFF)) {proxyVal = (dynamic_cast<RooRealVar*>(argFF))->getVal();}
     else { proxyVal = (dynamic_cast<RooFormulaVar*>(argFF))->getVal();}
     FFparamlist_previous.emplace(argFF->GetName(), proxyVal);
     
   }
  
   delete iterRe;
   delete iterIm;
   delete iterFF;

   return ;

 }
